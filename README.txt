#Follow The Money README.txt

## To Make Data Changes

###


## --- FOR DEVELOPMENT ONLY ---

#### TODO List:
::TODO::
:: Switch Checkbox functionality hookup
:: Fix background width
:: Naming States
:: Edit ProgramYearTip -- tip for year that a program starts
:: Link Main ReadMore link
:: Link Full Text Pages
:: Summary Sentence -- wait for intra-county description
:: Touch Sensing Test
:: Format Rankings

#### CHECKED List:
:: Fix full description button
:: Fix description, keytrends button
:: Replace switch map button with checkbox
:: Switch checkbox style
:: Refresh graph between programs
:: Set up no data & inelligible
:: Link New Data
:: Touch Sensing Fix
:: Link Rankings CSV
:: Inactive menu items
:: No county fix
:: Update Chart Legend color classes for intra county
:: Dash for underscore (traded for icon instead)
:: Keep current year consistent
:: Allign intersection of divs
:: 0-20% included in intra county map lowest color_class
:: empty legend div for chart @ empty selection
:: Country shadow
:: Highlight county
:: County rollover tooltip
:: Chart Label (current year)
:: Titles and program tooltips
:: Scale Chart tick marks
:: Check Tables for remaining maps (to ensure they include relevant ST_CNTY data that is consistent with the data tables)
:: Maintain Chart at all times (including init handling)
:: Current year handler
:: Create code for time controller
:: Create code for map controller (for switching maps)
:: Adjust CSS
:: adjust timechart dimensions**
:: Edit down maps to only include relevant States
:: Check if Custom CSS is properly Linked
:: Think of options for stagered datasets (multiple csv/tsv files)
:: Check in about the inconsistency in ST_CNTY Data
:: Adjust data loader for multiple decades **
:: Include min & max for each category of data in the line chart creation
	Think of a way to automate this?? ^
:: Edit scatterplot/linechart to reflected cross-decadal data

## --- DEVELOPER NOTES ---

States of Interest:
AZ - Arizona - FIPS 04
CA - California - FIPS 06
CO - Colorado - FIPS 08
ID - Idaho - FIPS 16
MT - Montana - FIPS 30
NM - New Mexico - FIPS 35
NV - Nevada - FIPS 32
OR - Oregon - FIPS 41
UT - Utah - FIPS 49
WA - Washington - FIPS 53
WY - Wyoming - FIPS 56

FIPS of Interest:
('040', '060', '080', '160', '300', '350', '320', '410', '490', '530', '560')

** Resources & Notes Used **

**** THIS WAS USED IN THE LATET VERSION ****
	Pulled down files from mapshaper.org
	NOTES for Creating Topojson Files:

	ogr2ogr \
		  -f GeoJSON \
		  -where "ADM0_A3 IN ('GBR', 'IRL')" \
		  subunits.json \
		  ne_10m_admin_0_map_subunits.shp

	ogr2ogr -f GeoJSON -s_srs albers.prj -t_srs EPSG:4326 wc_wgs84_geo.json Western_Counties_sm/wc.json

	topojson -o wc_wgs84_topo.json --id-property id -- wc_wgs84_geo.json

	RESOURCES USED:

	D3.CARTO.MAP :: https://github.com/emeeks/d3-carto-map/wiki/API-Reference


--- OTHER USEFUL INFORMATION ---

http://www.tnoda.com/blog/2013-12-07
	geojson
		ogr2ogr -f GeoJSON new_file_name.json shape_file.shp
		ogr2ogr \
		  -f GeoJSON \
		  -where "ADM0_A3 IN ('GBR', 'IRL')" \
		  subunits.json \
		  ne_10m_admin_0_map_subunits.shp
	topojson
		topojson \
		  -o uk.topo.json \
		  --id-property SU_A3 \
		  --properties name=NAME \
		  -- \
		  subunits.json \
		  places.json

	Actual Commands Used:
		>> GEOJSON:
		ogr2ogr -f GeoJSON -where "STATE IN ('040', '060', '080', '160', '300', '350', '320', '410', '490', '530', '560')" json_file.json shape.shp

		>> TOPOJSON:
		topojson -o topo_json_file.topo.json --id-property ST_CNTY --width=650 --height=460 -- json_file.json


		>> GEOJSON (broken up by attribute):
		ogr2ogr \
			-f GeoJSON \
			-where "STATE IN ('040', '060', '080', '160', '300', '350', '320', '410', '490', '530', '560')" \
			json_file.json \
			shape.shp

		>> TOPOJSON (broken up by attribute):
		topojson \
			-o topo_json_file.topo.json \
			--id-property ST_CNTY \
			--width=650 \
			--height=460 \
			-- json_file.json



Bubble Map Example (useful!!)
http://bost.ocks.org/mike/bubble-map/

FIPS: http://en.wikipedia.org/wiki/Federal_Information_Processing_Standard_state_code

Button Press Example:
http://bl.ocks.org/d3noob/7030f35b72de721622b8

Chloropleth Ex:
http://bl.ocks.org/mbostock/4060606
http://bl.ocks.org/fhernand/be1e9c9fdb0473292abf

Tutorial:
http://alignedleft.com/tutorials/d3

NY Times Example - The Geography of Government Benefits
http://www.nytimes.com/interactive/2012/02/12/us/entitlement-map.html?_r=0
